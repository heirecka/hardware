# Copyright 2009-2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2010-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2010 Piotr Jaorszyński <p.jaroszynski@gmail.com>
# Distributed under the terms of the GNU General Public License v2

# Note to my future self: Find status information here: http://www.nvidia.com/object/unix.html

require makeself alternatives

export_exlib_phases src_unpack src_prepare src_configure src_install pkg_postinst

SUMMARY="NVIDIA X11 driver and GLX libraries"
HOMEPAGE="https://www.nvidia.com/"

DOWNLOADS="
    listed-only:
        https://us.download.nvidia.com/XFree86/Linux-x86_64/${PV}/NVIDIA-Linux-x86_64-${PV}.run
        https://download.nvidia.com/XFree86/Linux-x86_64/${PV}/NVIDIA-Linux-x86_64-${PV}.run
        ftp://download.nvidia.com/XFree86/Linux-x86_64/${PV}/NVIDIA-Linux-x86_64-${PV}.run
"

BUGS_TO="philantrop@exherbo.org"

LICENCES="NVIDIA"
SLOT="0"
MYOPTIONS="
    doc
    tools [[ description = [ Install nvidia-settings GUI application ] ]]
    wayland
"

DEPENDENCIES="
    build:
        virtual/pkg-config [[ note = [ used in the exlib below ] ]]
    build+run:
        x11-server/xorg-server [[ note = [ used in the pkg-config check below ] ]]
    run:
        dev-libs/ocl-icd [[ note = [ provides OpenCL wrapper library ] ]]
        x11-libs/libX11
        x11-libs/libXau
        x11-libs/libXext
        tools? (
            dev-libs/atk
            dev-libs/glib:2
            x11-libs/gtk+:3
            x11-libs/pango
        )
        wayland? ( sys-libs/wayland )
    suggestion:
        x11-libs/libvdpau[>=0.3] [[ note = [ nvidia-drivers installs a VDPAU driver ] ]]
"

NVIDIA_DRIVER_MODULES=(
    "nvidia /kernel/drivers/video"
    "nvidia-drm /kernel/drivers/video"
    "nvidia-modeset /kernel/drivers/video"
    "nvidia-uvm /kernel/drivers/video"
)

nvidia-drivers_generate_dkms_modules() {
    local module n=0
    for module in "${NVIDIA_DRIVER_MODULES[@]}"; do
        local args=(${module})
        echo "BUILT_MODULE_NAME[${n}]=\"${args[0]}\""
        echo "DEST_MODULE_LOCATION[${n}]=\"${args[1]}\""
        let ++n
    done
}

nvidia-drivers-396_src_unpack() {
    unpack_makeself NVIDIA-Linux-x86_64-${PV}.run
}

nvidia-drivers-396_src_prepare() {
    local desktop_path=usr/share/applications/nvidia-settings.desktop
    desktop_path=nvidia-settings.desktop

    default

    edo sed \
        -e "s:__UTILS_PATH__:/usr/$(exhost --target)/bin:" \
        -e 's:__PIXMAP_PATH__:/usr/share/pixmaps:' \
        -i "${desktop_path}"
}

nvidia-drivers-396_src_configure() {
    moduledir=$($(exhost --target)-pkg-config --variable=moduledir xorg-server)
    [[ -z ${moduledir} ]] && die "Sanity check failed: \$($(exhost --target)-pkg-config --variable=moduledir xorg-server) returned nothing"

    # configure dkms
    local dkms_modules=$(nvidia-drivers_generate_dkms_modules | sed -e 's/$/\\/g;$s/\\$//')

    edo sed \
        -e "s:__VERSION_STRING:${PV}:" \
        -e "s:__JOBS:${EXJOBS}:" \
        -e "s:__EXCLUDE_MODULES::" \
        -e 's/^__DKMS_MODULES$/#&/' \
        -e "/^#__DKMS_MODULES$/a${dkms_modules}" \
        -i kernel/dkms.conf
}

src_install_common() {
    # docs
    dodoc pkg-history.txt NVIDIA_Changelog README.txt
    if option doc; then
        docinto html
        dodoc -r html/*
    fi
}

src_install_32bit() {
    # parse the .manifest file to figure out where to install stuff
    while read line ; do
        line=( $line )
        case ${line[2]} in
            UTILITY_LIB|ENCODEAPI_LIB|NVCUVID_LIB|GLX_CLIENT_LIB|EGL_CLIENT_LIB)
                if [[ ${line[3]} == "COMPAT32" ]] ; then
                    exeinto /usr/$(exhost --target)/lib
                    doexe ${line[0]}
                fi
                ;;
            UTILITY_LIB_SYMLINK|ENCODEAPI_LIB_SYMLINK|NVCUVID_LIB_SYMLINK)
                if [[ ${line[3]} == "COMPAT32" ]] ; then
                    dosym ${line[4]} /usr/$(exhost --target)/lib/${line[0]}
                fi
                ;;
            OPENGL_LIB|VDPAU_LIB|CUDA_LIB|GLVND_LIB|OPENCL_LIB|NVIFR_LIB)
                if [[ ${line[3]} == "COMPAT32" ]] ; then
                    exeinto /usr/$(exhost --target)/lib/${line[4]}
                    doexe ${line[0]}
                fi
                ;;
            VDPAU_SYMLINK|CUDA_SYMLINK)
                if [[ ${line[3]} == "COMPAT32" ]] ; then
                    dosym ${line[5]} /usr/$(exhost --target)/lib/${line[4]}/${line[0]}
                fi
                ;;
            TLS_LIB)
                if [[ ${line[3]} == "COMPAT32" ]] ; then
                    exeinto /usr/$(exhost --target)/lib/${line[5]}
                    doexe ${line[0]}
                fi
                ;;
        esac
    done < .manifest
}

src_install_64bit() {
    # we do not install nvidia-installer, remove the manpage
    edo sed -e '/nvidia-installer\.1\.gz/d' -i .manifest

    # use only gtk3 for nvidia-settings in recent versions
    edo sed -e '/libnvidia-gtk2.so/d' -i .manifest

    # remove disabled stuff
    if ! option tools ; then
        edo sed \
            -e '/libnvidia-gtk3.so/d' \
            -e '/nvidia-settings/d' \
            -i .manifest
    fi

    if ! option wayland ; then
        edo sed \
            -e '/10_nvidia_wayland.json/d' \
            -e '/libnvidia-egl-wayland.so/d' \
            -i .manifest
    fi

    # fill nvidia_icd.json.template and rename
    edo sed \
        -e 's:__NV_VK_ICD__:libGLX_nvidia.so.0:g' \
        -i nvidia_icd.json.template
    edo sed \
        -e 's:nvidia_icd.json.template:nvidia_icd.json:g' \
        -i .manifest
    edo mv nvidia_icd.json.template nvidia_icd.json

    # parse the .manifest file to figure out where to install stuff
    while read line ; do
        line=( $line )
        case ${line[2]} in
            UTILITY_BINARY)
                dobin ${line[0]}
                ;;
            MANPAGE)
                edo gunzip ${line[0]}
                doman ${line[0]%.gz}
                ;;
            XMODULE_SHARED_LIB|GLX_MODULE_SHARED_LIB)
                exeinto ${moduledir}/${line[3]}
                doexe ${line[0]}
                ;;
            XMODULE_SYMLINK|GLX_MODULE_SYMLINK)
                ;;
            XLIB_SHARED_LIB|UTILITY_LIB|ENCODEAPI_LIB|NVCUVID_LIB|GLX_CLIENT_LIB|EGL_CLIENT_LIB)
                if [[ ${line[3]} == "NATIVE" ]] ; then
                    dolib.so ${line[0]}
                fi
                ;;
            XLIB_SYMLINK|UTILITY_LIB_SYMLINK|ENCODEAPI_LIB_SYMLINK|NVCUVID_LIB_SYMLINK)
                if [[ ${line[3]} == "NATIVE" ]] ; then
                    dosym ${line[4]} /usr/$(exhost --target)/lib/${line[0]}
                fi
                ;;
            OPENGL_LIB|VDPAU_LIB|CUDA_LIB|GLVND_LIB|OPENCL_LIB|NVIFR_LIB)
                if [[ ${line[3]} == "NATIVE" ]] ; then
                    exeinto /usr/$(exhost --target)/lib/${line[4]}
                    doexe ${line[0]}
                fi
                ;;
            VDPAU_SYMLINK|CUDA_SYMLINK)
                if [[ ${line[3]} == "NATIVE" ]] ; then
                    dosym ${line[5]} /usr/$(exhost --target)/lib/${line[4]}/${line[0]}
                fi
                ;;
            TLS_LIB)
                if [[ ${line[3]} == "NATIVE" ]] ; then
                    exeinto /usr/$(exhost --target)/lib/${line[5]}
                    doexe ${line[0]}
                fi
                ;;
            DOT_DESKTOP)
                if option tools ; then
                    insinto /usr/share/applications
                    doins ${line[0]}
                fi
                ;;
            APPLICATION_PROFILE)
                if option tools ; then
                    insinto /usr/share/nvidia
                    doins ${line[0]}
                fi
                ;;
            XORG_OUTPUTCLASS_CONFIG)
                insinto /usr/share/X11/xorg.conf.d
                doins ${line[0]}
                ;;
            CUDA_ICD)
                insinto /etc/OpenCL/vendors
                doins ${line[0]}
                ;;
            VULKAN_ICD_JSON)
                insinto /etc/vulkan/icd.d
                doins ${line[0]}
                ;;
            GLVND_EGL_ICD_JSON)
                insinto /usr/share/glvnd/egl_vendor.d
                doins ${line[0]}
                ;;
            EGL_EXTERNAL_PLATFORM_JSON)
                insinto /usr/share/glvnd/egl_vendor.d
                doins ${line[0]}
                ;;
        esac
    done < .manifest

    # nvidia-settings icon
    if option tools ; then
        insinto /usr/share/pixmaps
        doins nvidia-settings.png
    fi

    # kernel module source
    insinto /usr/src/nvidia-drivers
    doins -r kernel/*

    local arch=x86_64

    edo mv "${IMAGE}"/usr/src/nvidia-drivers/nvidia/nv-kernel{,-${arch}}.o_binary
    edo sed \
        -e "s/nv-kernel\\.o/nv-kernel-\$(ARCH).o/" \
        -i "${IMAGE}"/usr/src/nvidia-drivers/nvidia/nvidia.Kbuild

    edo mv "${IMAGE}"/usr/src/nvidia-drivers/nvidia-modeset/nv-modeset-kernel{,-${arch}}.o_binary
    edo sed \
        -e "s/nv-modeset-kernel\\.o/nv-modeset-kernel-\$(ARCH).o/" \
        -i "${IMAGE}"/usr/src/nvidia-drivers/nvidia-modeset/nvidia-modeset.Kbuild

    hereenvd 40nvidia <<EOF
LDPATH="/usr/$(exhost --target)/lib/vdpau"
EOF

    if option tools ; then
        insinto /etc/X11/xinit/xinitrc.d
        hereins 95-nvidia-settings <<EOF
#!/bin/sh

/usr/$(exhost --target)/bin/nvidia-settings --load-config-only
EOF
        edo chmod ugo+x "${IMAGE}"/etc/X11/xinit/xinitrc.d/95-nvidia-settings
    fi
}

nvidia_alternatives() {
    local libs=( libGL libEGL libGLESv1_CM libGLESv2 )

    edo mkdir "${IMAGE}"/usr/$(exhost --target)/lib/opengl

    edo mv \
        "${IMAGE}"/usr/$(exhost --target)/lib/libEGL.so.1.1.0 \
        "${IMAGE}"/usr/$(exhost --target)/lib/libEGL.so.${PV}

    local modules=( egl opengl nvifr )
    for module in "${modules[@]}"; do
        edo mv "${IMAGE}"/usr/$(exhost --target)/lib/MODULE\:${module}/* \
            "${IMAGE}"/usr/$(exhost --target)/lib/
        edo rmdir "${IMAGE}"/usr/$(exhost --target)/lib/MODULE\:${module}
    done

    # The next line INTENTIONALLY overwrites the existing non-GLVND libGL.so.${PV}.
    # We want to support the OpenGL Vendor-Neutral Driver (GLVND) which will
    # ultimately free us from eclectic opengl.
    edo mv \
        "${IMAGE}"/usr/$(exhost --target)/lib/libGL.so.1.7.0 \
        "${IMAGE}"/usr/$(exhost --target)/lib/libGL.so.${PV}
    edo mv \
        "${IMAGE}"/usr/$(exhost --target)/lib/libGLESv1_CM.so.1.2.0 \
        "${IMAGE}"/usr/$(exhost --target)/lib/libGLESv1_CM.so.${PV}
    edo mv \
        "${IMAGE}"/usr/$(exhost --target)/lib/libGLESv2.so.2.1.0 \
        "${IMAGE}"/usr/$(exhost --target)/lib/libGLESv2.so.${PV}

    local lib path soname
    for lib in "${libs[@]}"; do
        path=/usr/$(exhost --target)/lib/opengl/${lib}-nvidia.so

        # rename the libraries so it isn't picked up by ldconfig as provider of lib*.so.*
        edo mv \
            "${IMAGE}"/usr/$(exhost --target)/lib/${lib}.so.${PV} \
            "${IMAGE}"/${path}

        soname=$($(exhost --target)-objdump -p "${IMAGE}"/${path} | sed -n 's/^ *SONAME *//p')

        alternatives+=(
            /usr/$(exhost --target)/lib/${lib}.so ${path}
            /usr/$(exhost --target)/lib/${soname} ${path}
        )
    done

    alternatives+=(${moduledir}/extensions/libglx.so{,.${PV}})
}

nvidia-drivers-396_src_install() {
    src_install_common

    local arch=$(exhost --target)

    case "${arch}" in
        i*86-*)
            src_install_32bit
            ;;
        *)
            src_install_64bit
            ;;
    esac

    local alternatives=()

    nvidia_alternatives

    alternatives_for opengl ${PN} 1 ${alternatives[@]}
}

nvidia-drivers-396_pkg_postinst() {
    alternatives_pkg_postinst

    elog "The kernel modules source is installed into /usr/src/nvidia-drivers/"
    elog "You will have to compile it by hand. Make sure the 'nvidia' kernel module is loaded."
    elog "Make sure you use the bfd linker to link the module, using gold"
    elog "seems to result in a broken module, so don't forget to run"
    elog "'eclectic ld set bfd' as root."
    elog "Do not forget to run 'eclectic opengl set nvidia-drivers' as root."
}

