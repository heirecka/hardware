# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]
require github [ user=rockowitz tag=v${PV} ]

SUMMARY="Query and change Linux monitor settings using DDC/CI and USB"
DESCRIPTION="
ddcutil is a program for querying and changing monitor settings, such as
brightness and color levels.
ddcutil uses DDC/CI to communicate with monitors implementing MCCS (Monitor
Control Command Set) over I2C. Normally, the video driver for the monitor
exposes the I2C channel as devices named /dev/i2c-n. There is also preliminary
support for monitors (such as Apple Cinema and Eizo ColorEdge) that implement
MCCS using a USB connection.
A particular use case for ddcutil is as part of color profile management.
Monitor calibration is relative to the monitor color settings currently in
effect, e.g. red gain. ddcutil allows color related settings to be saved at
the time a monitor is calibrated, and then restored when the calibration is
applied."

HOMEPAGE+=" http://www.ddcutil.com/"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}release_notes/"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""


DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.14]
        dev-libs/libusb:1[>=1.0.15]
        sys-apps/systemd
        x11-dri/libdrm[>=2.4.67]
        x11-libs/libXrandr
        x11-libs/libX11
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # https://github.com/rockowitz/ddcutil/issues/13
    --disable-doxygen
    --disable-swig
    --enable-drm
    --enable-lib
    --enable-usb
)

